
package entidades;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.servlet.http.HttpServletResponse;


public class EquipoDAO extends BD{
    
    public static ArrayList<Equipo> Listado(){
        
        ArrayList<Equipo> listado=new ArrayList<>();
        Connection con;
        
        try {
            con = BD.getConnection();
            PreparedStatement consulta = con.prepareStatement("Select * from clubesdeprimera");
            ResultSet resultado = consulta.executeQuery();
            while(resultado.next()){
                Equipo equipo=new Equipo();
                
                equipo.id = resultado.getInt("id");
                equipo.equipos = resultado.getString("Equipos");
                equipo.barrios = resultado.getString("Barrios");
                equipo.capitanes = resultado.getBinaryStream("Capitan");
                
                listado.add(equipo);
            }
            resultado.close();
            consulta.close();
            con.close();
        } catch (Exception e) {
            System.err.println("SQLException: " + e.getMessage());
           return null;
        }
        
        return listado;
    }
    
    public static void listarImg(int id,HttpServletResponse response){
        
        Connection con;
        InputStream inputStream=null;
        OutputStream outputStream=null;
        BufferedInputStream bufferedInputStream=null;
        BufferedOutputStream bufferedOutputStream=null;
        response.setContentType("image/*");
        
        try {
            outputStream=response.getOutputStream();
            con = BD.getConnection();
            PreparedStatement consulta = con.prepareStatement("select * from clubesdeprimera where id="+id);
            ResultSet resultado = consulta.executeQuery();
            
            if(resultado.next()){
                inputStream=resultado.getBinaryStream("Capitan");
            }
            bufferedInputStream=new BufferedInputStream(inputStream);
            bufferedOutputStream=new BufferedOutputStream(outputStream);
            int i=0;
            while((i=bufferedInputStream.read())!=-1){
                bufferedOutputStream.write(i);
            }
        } catch (Exception e) {
        }
    }
}
